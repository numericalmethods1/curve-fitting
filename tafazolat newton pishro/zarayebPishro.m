% IN THE NAME OF ALLAH (GOD)
% Written by Mahdi  2021 (https://gitlab.com/Mahdiali313) or github
% tafazolat Pishro newton
% see more detail on (https://gitlab.com/numericalmethods1)
function resualt = zarayebPishro (y)

r = rot90(y,3); %r means resualt

%---------------------calculate----------------------
rows = length(y);
j=1;

while rows ~= 0
    
for i= 1:rows-1
   r(i,j+1) = (r(i+1,j) - r(i,j));
end

j=j+1;
rows=rows-1;
end

resualt=r;
end
