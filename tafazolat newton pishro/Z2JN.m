% IN THE NAME OF ALLAH (GOD)
% Written by Mahdi  2021 (https://gitlab.com/Mahdiali313) or github
% Z2JN = zarayeb 2 jomle ee Newton
% see more detail on (https://gitlab.com/numericalmethods1)
function r = Z2JN(s,n)

if n==0
    r=1 ;
    return
end

r=s/factorial(n);

for i=2:n
   r= r* (s-1);
   s=s-1;
end

end