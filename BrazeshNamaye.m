% IN THE NAME OF ALLAH (GOD)
% Written by Mahdi  2021 (https://gitlab.com/Mahdiali313)
% Brazesh Namayee
% see more detail on (https://gitlab.com/numericalmethods1)
clear
clc
format short

%---------------------input----------------------
disp("enter X")
x = input ('');
%x=[1 3 4 6 9 15];

disp("enter Y")
y = input ('');
%y=[4 3.5 2.9 2.5 2.75 2];
Y=log(y);%Ln(y)

%---------------------calculate----------------------
%calculate Amatrix = matrix of sigma X ha
for j=0:1 
for i=j:1+j
   Amatrix(j+1,i-j+1)=sum(x.^(i));
end
end

%calculate Bmatrix = matrix of sigma Y ha
for i=0:1
    Bmatrix(i+1,1)=sum(Y.*x.^(i));
end

Xmatrix=ghosFunc(Amatrix,Bmatrix,10^(-3));
a=Xmatrix(1);
b=Xmatrix(2);
c=exp(a);
Px=@(x)c*exp(b*x);

%---------------------resualt----------------------
clc
disp(['x= ',num2str(x)])
disp(['y= ',num2str(y)])
disp(['Y= ',num2str(Y)])
disp(['a=',num2str(a),'  b=',num2str(b),'  c=',num2str(c)])

plot(x,y);hold on
bazeX=[x(1) x(length(x))];
fplot(Px,bazeX,'.-'),legend('X|Y', 'P(x)');
