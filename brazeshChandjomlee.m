% IN THE NAME OF ALLAH (GOD)
% Written by Mahdi  2021 (https://gitlab.com/Mahdiali313)
% Brazesh 
% see more detail on (https://gitlab.com/numericalmethods1)
clear
clc
format short
%---------------------input----------------------
disp("enter X")
X = input ('');
%X=[1 3 4 5 6 7];%X=[1 2 3 4]%

disp("enter Y")
Y = input ('');
%Y=[2 7 8 10 11 11];%Y=[1 1.5 1.75 2]%

N=length(X);
disp(['N=',num2str(N)])

disp('enter m (daraje . N-1 > m)')
m= input ('');
%m=2;
if m >= N-1
    error('m must lower than N-1')
end


%---------------------calculate----------------------

%calculate Amatrix = matrix of sigma X ha
for j=0:m
    
for i=j:m+j
   Amatrix(j+1,i-j+1)=sum(X.^(i));
end

end

%calculate Bmatrix = matrix of sigma Y ha
for i=0:m
    Bmatrix(i+1,1)=sum(Y.*X.^(i));
end

Xmatrix=ghosFunc(Amatrix,Bmatrix,10^(-3));

%---------------------resualt----------------------
clc
disp(['X= ',num2str(X)])
disp(['Y= ',num2str(Y)])
disp(['a = ',num2str(rot90(Xmatrix))])

%---------------------PLOT----------------------
plot(X,Y);hold on

% make P(x) with Xmatrix and draw P(x)
Pstr=['@(x)',num2str(Xmatrix(1))];
for k=2:length(Xmatrix)
     new=['+',num2str(Xmatrix(k)),'*x^', num2str(k-1)];
     Pstr=strcat(Pstr,new);
end
disp(['P(x)=',Pstr])
Pstr=str2func(Pstr); %convert from str to function_handler
bazeX=[X(1) X(length(X))];
fplot(Pstr,bazeX,'.-'),legend('X|Y', 'P(x)');brazeshChandjomlee.m
